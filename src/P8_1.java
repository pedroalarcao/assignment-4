import java.util.Scanner;


class ComboLock{

    private int dial = 0;

    private final int secret1;
    private final int secret2;
    private final int secret3;

    boolean unlocked1 = false;
    boolean unlocked2 = false;
    boolean unlocked3 = false;
    
    public ComboLock(int secret1, int secret2, int secret3){

        this.secret1 = secret1;
        this.secret2 = secret2;
        this.secret3 = secret3;
    }

    public void reset(){

        dial = 0;
        unlocked1 = false;
        unlocked2 = false;
        unlocked3 = false;

    }

    public void turnLeft(int ticks){

        //since when turning left we are in ascending order, we just have to make sure we stay between 0-39
        dial = (dial + ticks) % 40;
        if (unlocked1 && dial == secret2){
            unlocked2 = true;
        }

    }

    public void turnRight(int ticks){

        //make sure that when ticks exceeds 40, it basically resumes from 0. When ticks - 40 it basically gives the reverse side of lock which works since turn right turns descending order.
        dial = (dial + (40 - ticks % 40))%40;

        //case where the dial is pointed at the first pass-number and also if third pass-number is same as first pass-number, this won't run
        if (dial == secret1 && unlocked1 == false){
            unlocked1 = true;
        }
        //case where we have already gotten second pass-number and we hit the right third pass-number
        if (unlocked2 && dial == secret3){
            unlocked3 = true;
        }

    }

    //will return true if unlocked1, unlocked2 and unlocked3 are true (means lock is open)
    public boolean open(){

        return unlocked1 && unlocked2 && unlocked3;
    }
}
public class P8_1 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter you three numbers between 0 and 39 separated by a space to be your combination lock: ");
        String inputValues = input.nextLine();

        //.split will separate values whenever a space is seen
        String[] secrets = inputValues.split(" ");

        int secret1 = Integer.parseInt(secrets[0]);
        int secret2 = Integer.parseInt(secrets[1]);
        int secret3 = Integer.parseInt(secrets[2]);

        ComboLock lock = new ComboLock(secret1, secret2, secret3);

        lock.reset();

        System.out.println("Lock is set and pointing at 0!");



        System.out.println("Please enter the number of ticks you'll like to turn right: ");
        int ticks = input.nextInt();
        lock.turnRight(ticks);
        System.out.println("Please enter the number of ticks you'll like to turn left: ");
        ticks = input.nextInt();
        lock.turnLeft(ticks);
        System.out.println("Please enter the number of ticks you'll like to turn right: ");
        ticks = input.nextInt();
        lock.turnRight(ticks);

        if (lock.open()){
            System.out.println("Success!");
        }
        else{
            System.out.println("Failed!");
        }




    }
}
