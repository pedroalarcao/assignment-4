import java.util.Scanner;
import java.lang.Math;

class SodaCan{

    public double getVolume(String[] values){

        //parse double will convert string values into double numbers
        double heightValue = Double.parseDouble(values[0]);
        double diameterValue = Double.parseDouble(values[1]);

        //volume formula for cylinder
        return (Math.PI * ((diameterValue/2)*(diameterValue/2)) * heightValue);
    }

    public double getSurfaceArea(String[] values){

        String height = values[0];
        String diameter = values[1];
        double heightValue = Double.parseDouble(height);
        double diameterValue = Double.parseDouble(diameter);

        //surface area formula for cylinder
        return (((2 * Math.PI) * (diameterValue/2) * heightValue) + ((2 * Math.PI) * ((diameterValue/2)*(diameterValue/2))));
    }
}

public class E4_23 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Enter Soda Can’s height and diameter, use space to separate elements:");
        String inputValues = input.nextLine();

        //.split will separate values whenever a space is seen
        String[] values = inputValues.split(" ");

        SodaCan can = new SodaCan();

        //%.2f will format with 2 decimal places
        System.out.println("Soda Can Volume: " + String.format("%.2f", can.getVolume(values)) + '\n' + "Soda Can Area: " + String.format("%.2f", can.getSurfaceArea(values)));

    }
}
